#Pierre Léveillé, 18 décembre 2018

Le design et la palette de couleur :

Je me suis inspiré d'un design simple qu'on retrouve de plus en plus sur le web et les réseaux sociaux comme instagram. La palette de couleurs est plutôt simple, c'est du blanc, du bleu, du gris. Le but étant de créer un effet propre, aéré, épuré, et léger. Ne pas trop charger de couleur avec de grands backgrounds, permet de se sentir plus zen lorsqu'on parcourt les pages.

On retrouve plusieurs exemples sur www.cargocollective.com, comme le site suivant : 
https://michelechampagne.com/

Ensuite j'ai choisi la police de caractère, et les images en fonction du thème de l'informatique. Avec un clavier virtuel, une carte-mère quelconque, la police Courier. Ainsi on embarque plus dans le thème.


L'image illustrator

Je me suis laissé inspirer pendant que j'ai fait les modifications. J'aimais bien le logo original du CVM, alors je l'ai importé, vectorisé, et puis j'ai fait ces quelques modifications en jouant avec les calques et les effets.

Les exigences du professeur

- Des tests de compatibilité ont été fait avec tous les navigateurs.
- Les hyperliens fonctionnent entre les pages.
- Le site web est responsive avec son menu, il change de couleur menu tablette et menu cellulaire avec un autre format mieux adapté. Aussi, dans la page travaux, les grid diminuent de colonnes par lignes.
-L'utilisation de différents formats de display ont été faits dans différentes pages :
    - flex, 
        dans la page travaux, nested, à ligne 536 du fichier SCSS, les textes dans les encadrés.
        dans la page régulier, nested, sur des images logo de code.
    - float
        dans la page index, comment je place mes widgets
        dans la page enseignants, comment je place mes widgets
    - grid
        dans la travaux, les travaux sont présentés en forme de grid
        dans le responsive je recalibre les propriétés

-Je me suis assuré que mon CSS et HTML étaient valides : 
	http://jigsaw.w3.org/css-validator/
	https://validator.w3.org/


Mes propriétés influencées par une transition CSS.
-  	les liens dans le menu avec la couleur qui transitionne
    source d'inspiration : https://www.creativebloq.com/use-css-transitions-link-effects-9134433 
-	l’image du logo qui disparait et apparait avec une image en arrière
    source d'inspiration
    http://css3.bradshawenterprises.com/cfimg/ 

J'ai utilisé des pseudoclasses dans mon CSS avec :
   - a.siteMenuLink:hover ligne 57
   - a.siteMenuLink:visited ligne 49
   - .logoImg img.top:hover ligne 53
  
Et finalement, l'utilisation de git et de SASS a été fait. :-)






